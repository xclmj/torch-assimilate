torch-assimilate package
========================

Assimilation algorithms
-----------------------
.. toctree::
   :maxdepth: 2

   assimilation

Localization
------------
.. toctree::
   :maxdepth: 2

   localization

Models
------
.. toctree::
   :maxdepth: 2

   model

Observation operators
---------------------
.. toctree::
   :maxdepth: 2

   obs_ops

Testing
-------
.. toctree::
   :maxdepth: 2

   testing

Observation subset
------------------
A :py:class:`xarray.Dataset` accessor, accessible with
:py:attr:`xarray.Dataset.obs`.

.. automodule:: pytassim.observation
    :members:
    :undoc-members:
    :show-inheritance:

Model state
-----------
A :py:class:`xarray.DataArray` accessor, accessible with
:py:attr:`xarray.DataArray.state`.

.. automodule:: pytassim.state
    :members:
    :undoc-members:
    :show-inheritance:
