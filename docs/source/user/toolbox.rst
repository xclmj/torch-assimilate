Variational inference toolbox
=============================


Variational autoencoder
-----------------------

.. autosummary::
    pytassim.toolbox.autoencoder.Autoencoder


Discriminators
--------------

Standard discriminator
^^^^^^^^^^^^^^^^^^^^^^


.. autosummary::
    pytassim.toolbox.discriminator.standard.StandardDisc


Regularizations
---------------

Gradient penalty
^^^^^^^^^^^^^^^^

.. autosummary::
    pytassim.toolbox.gradient_penalty.zero_grad_penalty


Utilities
---------

Loss wrapper
^^^^^^^^^^^^


Heun's method
^^^^^^^^^^^^^


.. autosummary::
    pytassim.toolbox.loss.LossWrapper
    pytassim.toolbox.heun.HeunMethod
